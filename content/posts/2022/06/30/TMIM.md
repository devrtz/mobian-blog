+++
title = "This month in Mobian: June 2022"
date = 2022-06-30T15:00:00+00:00
tags = ["mobian", "pinephone"]
+++

**The end is near!** The end of June at least, and that means it is time for our monthly recap of what has happened.

<!--more-->

# Monthly dev meeting

Many mobian devs came together on the first Saturday of June. Topics of the
meeting included:

- moving repositories to salsa: everything from mobian/packages, around 60 repos in total; planning continues, security considerations (access to ssh/signing keys etc needs to be considered); we still need to decide on a switch-over date.

- Matrix room upgrade: We need to upgrade our #mobian room to a newer version, as the old one is not compatible with the latest matrix servers anymore. The only problem are the bridges to our other channels. spaetz takes care of the IRC bridge, nobody knows who the telegram bridge admin is, it should not be a blocker though.
  *Update:* of course the update did not go as well as planned, and people had to migrate manually to the new room. Eventually we got everyone over though.

- We might replace gedit with gnome-text-editor, it is more enjoyable. And it would allow us to drop our custom gedit package.
  *Update:* This has now happened.

# Calamares
Calamares is the installer that we use to install Mobian. It was finally updated in Debian proper (thanks!), and as a result we were able to drop our Mobian-specific package. In Mobian remaining: calamares-extensions 	1.2.1-1 and calamares-settings-mobian  0.2.3

# Mobian, now with even more DDs!

No, DD is not shorthand for Donald Duck, it stands for "Debian Developer" which is a formal status, requiring sponsors and a (usually) lengthy process. We have quite a few DDs in our own (informal) ranks, but Mobian's founder @a-wai has now also [become one](https://mastodon.online/@awai/108561758730576102). Congrats Arnaud, and way to go!

# gnome-text-exitor, now more mobile friendly

We have [added patches](https://gitlab.gnome.org/GNOME/gnome-text-editor/-/merge_requests/52) to make gnome-text-editor more mobile friendly, by wrapping labels on narrow screens in the top bar. Patches have been sent to upstream too.

# PinePhone Keyboard
The PinePhone keyboard driver in the 5.15-sunxi64 kernel has been fixed, allowing both the keyboard and battery levels to work again thanks to testing by Johns in #Mobian who provided logs and validated the fix pre-release. You may notice that the function keys no longer work the way they used to. A configuration which fixes this permanently can be found on the [Mobian wiki](https://wiki.mobian.org/ppaccessories).

# PGP Key Expiry
Like certificates before certbot, you never think about PGP key expiry until it bites. Mobian has just celebrated it's two year repository key anniversary the fun way: by scrambling to update the key and get it out to devices. Fear not, the key has not been compromised. The same old key with an updated expiry can be found on the [Mobian repository](https://repo.mobian.org/mobian.gpg) and your system can be updated with it by running `apt-key --keyring=/etc/apt/trusted.gpg.d/mobian.gpg mobian.gpg` after downloading the file. 
We will have a new package called `mobian-archive-keyring` which will be pulled in automatically by `mobian-base` in order to update the gpg key automatically the next time.

The new key:
```
> $ sha256sum mobian.gpg*
4ab90ff82a88f11f681e5e857503833eb2108c9a77edaa9f64b7648c1b91c60a  mobian.gpg
e1004f14f260ba2835e80a6a4c3d4bc48a3db264bed93fe32438a2a50394516b  mobian.gpg.key

> $ gpg --show-key < mobian.gpg
pub   rsa4096 2020-06-16 [SC] [expires: 2024-06-15]
      D569936C7E32F193CBAAEC48393F924A855FB27D
uid                      Mobian Project <admin@mobian-project.org>
sub   rsa4096 2020-06-16 [S] [expires: 2024-06-15]
```

The old key:
```
$ sha256sum mobian_old.gpg*
659f04acdd8226032a086ec054926ddf8c8207f64e092365955c8740471fd309  mobian_old.gpg
e52c1b41c5e52d1932ec9bd009d20abdb0b5cab01a89ddc51237adde28dcdc3f  mobian_old.gpg.key

> $ gpg --show-key < mobian_old.gpg
pub   rsa4096 2020-06-16 [SC] [expired: 2022-06-16]
      D569936C7E32F193CBAAEC48393F924A855FB27D
uid                      Mobian Project <admin@mobian-project.org>
sub   rsa4096 2020-06-16 [S] [expired: 2022-06-16]
```

# Kernel status

| Mobile            | Version       |
|:----------------- |:-------------:|
|linux-5.15-librem5 | 5.15.37+librem5-7 |
|linux-5.15-sunxi64 | 5.15.50+sunxi64-2 |
|linux-5.17-arm64-mobian | 5.17.5+mobian-2 |
|linux-5.17-librem5 | 5.17.5+librem5-2 |
|linux-5.17-rockchip| 5.17.15+rockchip-1 |
|linux-5.17-sdm845 	| 5.17.5+sdm845-5 |
|linux-5.17-sunxi64 | 5.17.1+sunxi64-1 |

Major kudos to @undef for tirelessly porting patches and resolving issues.

# Dropping packages

Quite a few custom packages are about to be updated in Debian, which will allow us to drop them from the Mobian repository in the near future.
* Calamares has already been dropped.
* Osk-sdl will transition to testing early next month.

# Upgrades
<!-- just include "highlights" here or anything we deem noteworthy -->
- 2022-06-01 calamares 3.2.59-1 into Debian unstable
- [2022-06-01](https://tracker.debian.org/news/1330138/accepted-phosh-0200beta1-1-source-into-experimental/) phosh 0.20.0~beta1-1 into Debian experimental, bringing a swipable keyboard (no longer acidentally tapping the bottom bar!) kudos to purism (and on June 27, we got 0.20.0-beta2 in experimental)

A full list on mobile-related uploads to Debian can be found in the corresponding [mailing list archive](https://alioth-lists.debian.net/pipermail/debian-on-mobile-maintainers/2022-June/date.html).
