+++
title = "These... past 4 months in Mobian: July - October 2022"
date = 2022-11-01T00:00:00+00:00
tags = ["mobian", "pinephone"]
+++

As ~~we're reaching the end of the month~~ we *slightly* overshot the month,
it's time to look back at what happened since July.

<!--more-->

# Monthly dev meeting

Highlights:
- puremaps packaging is progressing. Some of the dependencies are now in Debian unstable,
  clearing the way for the main package.
- The image resizing bug has been solved by switching to `systemd-repart` and `systemd-growfs`.
  @undef was instrumental in this switch by contributing several fixes upstream to make things
  work smoothly.
- Currently working on the best way to support config fragments in `u-boot-menu`
  ([#1012333](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1012333)) to support encrypted
  PinePhone Pro installs and PinePhones with the VCCQ mod.

# Everyone loves salsa!

We completed the move to Salsa, though there were a few issues along the way:
- kernels couldn't be built through CI due to limits in artifacts size, forcing us to create a
  simpler, different pipeline for those packages
- the issue tracker remained on gitlab.com for a while, until we decided on the best way to handle
  this migration

These problems have now been solved, meaning all packages are now maintained and built on Salsa. We
also decided to get rid of the single issue tracker and instead ask users to open issues in the
corresponding repo/project.

Finally, we won't be migrating issues from gitlab.com as they would then lose all authorship
information. We therefore ask users who had previously opened issues on gitlab.com to create new
issues on Salsa, notifying them by leaving a comment on existing issues explaining the way forward
([example](https://gitlab.com/mobian1/issues/-/issues/1#note_1144472496)).

# Kernel status

Due to a desire to ship a kernel with a working camera the 5.18 release cycle was skipped for most
devices. 5.19 on the other hand has been released shortly after upstream, and most device families
can now even use 6.0-based kernels, although this is not the default yet.

Those more recent kernels contains a number of improvements including:
* Working camera on the PinePhone Pro (using a script like https://salsa.debian.org/-/snippets/611,
  no Megapixels support yet)
* Improved USB-C handling (should now charge and support docks/DP alt mode better) on the PinePhone
  Pro as well
* The (temporary) death of the PinePhone Pro smurfbug
* Better audio support on SDM845-based devices

As is the eternal fun of maintaining downstream patches, the update to Linux 6.0 has killed the
main camera on the PinePhone Pro. We hope to work out this bug and get it back to working state
shortly.

# Updates and new packages in Debian

[qrtr](https://tracker.debian.org/pkg/qrtr), a set of userspace utilities needed for Qualcomm-based
phones has reached Debian, as well as a number of other packages:

* [eg25-manager](https://tracker.debian.org/pkg/eg25-manager)
* [numberstation](https://tracker.debian.org/pkg/numberstation)
* [phog](https://tracker.debian.org/pkg/phog)
* [phosh-antispam](https://tracker.debian.org/pkg/phosh-antispam)
* [plymouth-theme-mobian](https://tracker.debian.org/pkg/plymouth-theme-mobian)
* [powersupply-gtk](https://tracker.debian.org/pkg/powersupply-gtk)

The firmware for the RTL8723CS (Wi-Fi/BT chip used on the OG PinePhone and PineTab) has also been
uploaded to Debian, although not by a member of the Mobian team. That obviously won't prevent us
from using this package and dropping our own packaging and getting another step closer to Debian
\o/.

`phog` is a new greeter designed for GTK based phones and Phosh, relying on
[greetd](https://git.sr.ht/~kennylevinsen/greetd) to perform the actual authentication and session
startup. This allows Phosh (or any other graphical environment) to be started in very much the same
way a "classical" desktop environment would start on any distribution, allows one to use multiple
user accounts (or even delete the `mobian` user and create another one) and automatically unlocks
the user's keyring on Phosh & GNOME.

# SXMO

The packages required for using SXMO on Mobian have been created and uploaded to Debian. See the
[Readme.Debian](https://sources.debian.org/src/sxmo-utils/1.11.1-2/debian/README.Debian/) file for
installation instructions. This brings a second Mobile UI to Debian, with Plasma Mobile and Lomiri
also well on the way.

# DebConf 22

This year's [DebConf22](https://debconf22.debconf.org/) in Kosovo was a blast!
There has been both upstream development done in and/or inspired by the trip:

Some notable mentions include
[Showing tickets on the lockscreen](https://social.librem.one/@agx/108612705281206357),
[automatically switching to high contrast based on ambient light sensor](https://social.librem.one/@agx/108668794144125214)
in `phosh`, [UX improvements](https://git.sr.ht/~fabrixxm/confy/log) in `confy`
[significantly faster startup](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/576) and
[being able to initiate sending a SMS from the history popover](https://gitlab.gnome.org/GNOME/calls/-/merge_requests/579)
in `gnome-calls` and probably more.

On the packaging front there have been a few uploads and the closing of the occasional bug :)
We're also excited that we got closer to having parts of the `sxmo` world during this thime!
For information about individual packages updates have a look at the list above and as usual on
the mailing list mentioned at the end of this blog post.

Honorary mention of this must-see talk (you've probably seen it by now) by agx about
[the current state of Debian on smartphones](https://debconf22.debconf.org/talks/103-the-current-state-of-debian-on-smartphones/)!

Finally we want to give a shout out to all of the nice people from the community we had the
pleasure to meet during DebCamp and DebConf, we had a very good time and of course also thanks to
everyone who made this event possible <3

Special mention to everyone working on the mobile sprint and to everyone who expressed interest in
having Debian mobile devices and chatted with us about it over a Rakija. Gëzuar!

# The GNOME 43 transition is over, the PipeWire transition begins

As you may have noticed, this summer has been sometimes a bit bumpy regarding packages/upgrades
availability and the overall stability of Mobian. This was in large part due to the ongoing Debian
transition to GNOME 43, involving a number of breaking changes. Thanks to the work of the Debian
GNOME team this transition was completed during the month of September and things have returned to
normal for a few weeks.

Similarly, as we all aim for `bookworm` to be a high-quality Debian release, it felt best for our
users if Debian switched to using PipeWire (and WirePlumber) as our default sound/multimedia server
(fully replacing PulseAudio) *before* the bookworm freeze expected to start next January. This is
definitely a change for the better and one that should improve a lot of things over time, including
for mobile users (call audio, BlueTooth audio and camera handling being just a few areas where we
can expect such improvements).

However, the move to PipeWire, while being mostly painless, can cause several issues and possibly
disrupt call audio handling. We have no intent to switch back to PulseAudio (it served us well for
a time, but we have to look forward!) and will instead work towards solving the current issues with
PipeWire. Therefore, we ask for your patience (and help if you feel like it) until the transition
to PipeWire is complete.

# Upgrades
<!-- just include "highlights" here or anything we deem noteworthy -->

As always, we do our best to keep up with upstream releases and have updated a number of Debian
packages over the past few months, including (but not limited to):

* [chatty](https://tracker.debian.org/pkg/chatty), now with Matrix support!
* [gnome-calls](https://tracker.debian.org/pkg/gnome-calls)
* [megapixels](https://tracker.debian.org/pkg/megapixels)
* [phoc](https://tracker.debian.org/pkg/phoc)
* [phosh](https://tracker.debian.org/pkg/phosh)
* [squeekboard](https://tracker.debian.org/pkg/squeekboard)
* [tootle](https://tracker.debian.org/pkg/tootle) could finally migrate to testing after fixing
  release-critical bugs

A full list on mobile-related uploads to Debian can be found in the corresponding
[mailing list archive](https://alioth-lists.debian.net/pipermail/debian-on-mobile-maintainers/2022-July/date.html).

# A note on Monthly posts

While we strive to get these blog posts out monthly, we are a small group of volunteers. We will
always prioritize getting updates and bugfixes to you over these posts, so please don't take a few
weeks/months of inactivity here as a sign of inactivity in Mobian itself.
