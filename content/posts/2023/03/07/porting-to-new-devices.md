+++
title = "Porting Mobian to New Devices"
date = 2023-03-07T00:00:00+00:00
tags = ["mobian", "porting", "devices"]
+++

As Mobian (and Mobile Linux in general) has gotten more popular, many folks have
started to ask if Mobian supports their particular phone. While we wish we lived in
a world where installing Mobian to a new phone model was just as easy as installing
Debian to a new laptop, things are (unfortunately) much more complicated than that.

### But my phone runs Android, and Android runs on Linux!

It is true that Android is Linux-based, but Android Linux diverged from Mobile Linux
a long time ago. Android Linux images are simply incompatible with Mobile Linux,
unless several compatibilty layers are installed.

In addition, the Android kernel shipped by manufacturers are based on LTS kernels,
and consist of millions of lines of [out of tree code](https://not.mainline.space/).
Much of this code cannot be upstreamed due to code quality, and sometimes the
code is of dubious legal origin. The kernel that the phone is based off of
is often ancient. In some cases, current devices might ship a version of Linux 3.19
which was released in 2015 and no longer gets updates from upstream.

This poses two problems for us: 1) they are full of security flaws and 2) their
API is oldish and different from what current Mobile Linux environments need (both
because it is old API and because it provides Android-specific API).
One would be stuck with a ancient and unmaintainable kernel that one could not
improve and a kernel that provides an incompatible interface to Mobile Linux.

Some distributions make that tradeoff and use these compatibility layers to bridge
the interface between Android kernels and Mobile Linux. One such is libhybris.
They basically choose to live with a kernel that one cannot modify and improve
in a significant manner. In addition, many kernels are [device](https://github.com/orgs/LineageOS/repositories?q=kernel)
specific[^1], which in turn creates a higher maintenance burden for every phone
added. Adding the compatibility layer does provide an advantage in that the
distribution can port to many more phones much quicker.

Mobian's ultimate goal is to erase the differences between Mobian and Debian.
As such, we do not wish to use compatibilty layers to get more phones working. We
would prefer to use a compatible kernel. As a consequence, we tend to work on
devices that align with our goals, or devices that have already have good mainline
support. However, even with this support, many devices lack mainline drivers to
give a good out of the box experience. Many of these phones cannot even boot
a mainline kernel, much less provide any usefulness to an end user.

### So how can I tell if my phone can even run Mobile Linux?

Our friends at [postmarketOS](https://wiki.postmarketos.org/wiki/Devices) have
a great wiki on devices that are supported and what features on those phones are
supported. If your device is not on this list, then most likely no one has even
tried to figure out if it can run on Mobile Linux (or porting it over is impossible).

### Ok, I want to port Mobian to my phone!

Great! We are happy to guide on how to do this. However, please understand this
is not a task that will be fast. The PinePhone and PinePhone Pro have a
vibrant community working to bring Mobile Linux to their phones, and
even after years of work, it is incomplete.

If you wish to port over a new device with Mobian, please come chat with us
[here](https://matrix.to/#/#mobian-ports:matrix.org) and we can assist you.
Please understand that should you choose to do this, we are asking you to keep
maintaining it.

### Can I give you my phone and then work on it?

Mobian's development team are mainly volunteers, and quite simply do not have the
time to port new devices. If you wish for your phone to run Mobian, the best
option is to start working on the porting process yourself.

### So what is my best option to run Mobile Linux?

If you want to use your current phone, some distributions, such as
[Ubuntu Touch](https://devices.ubuntu-touch.io/) and
[Droidian](https://devices.droidian.org/devices/), have a compatibility layer to
work with Android Linux kernels, which can be an option to get into things but
brings in the downsides of outdated Android kernels as explained above.

Your best option however is to purchase a phone that has good Mobile Linux support.
Please support device manufacturers that try to upstream their devices to mainline
Linux! Though Purism's [Librem 5](https://puri.sm/products/librem-5/) is expensive,
they actively put in work to upstream their phone to mainline Linux (and greatly
contribute to Mobile Linux). The Pine64 [Pinephone](https://www.pine64.org/pinephone/)
and [Pinephone Pro](https://www.pine64.org/pinephonepro/) has great community
support to bring Mobile Linux to their devices.

The community has also put in a lot of effort to mainline some phones, so buying
one of those phones is also a good option. See [here](https://wiki.mobian-project.org/doku.php?id=devices)
for a list of supported phones.

## Final words

While we would really enjoy living in a world where installing Mobian on a phone
is as easy as installing Debian on a Desktop/Laptop, the reality is that many
phone manufacturers do not make any attempts to help with this goal. Porting
a new device to Mobile Linux takes a lot of time and effort, even with the support
of a large community.

Author's note: A big thank you to the [postmarketOS](https://postmarketos.org/) folks
for helping out with this post!

[^1]: Sometimes the kernel can be SoC and Vendor specific. For example, there's a generic downstream sdm845 LG kernel, but there's no generic downstream LG kernel or generic sdm845 kernel.
