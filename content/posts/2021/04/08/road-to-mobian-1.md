+++
title = "The road to the Mobian Community Edition (part 1)"
date = "2021-04-08"
tags = ["mobian", "pinephone", "pine64"]
author = "a-wai"
+++

The Mobian project was officially announced on May 14th, 2020. But those who
witnessed its birth surely remember it all started a few months prior to this
date, a bit more than one year ago, actually. One year hacking and building both
a mobile operating system and a community. One year refining our system, getting
rid of the early days' hacks in favor of more mature and robust solutions.
One year ending with a major milestone for such a young project: getting the
[Mobian Community Edition PinePhone]( {{< ref "mobian-community-edition.md" >}} )
into the hands of thousands of users. One hell of a year, indeed!

<!--more-->

It all really started on January 29th, 2020, when I received a shiny new toy:
an ARM64 development system packed with a number of interesting, if not unique,
features:

* 5.95" touchscreen
* removable battery
* integrated eMMC
* µSD socket
* USB-C port
* 3.5" headset jack
* 4G modem

The [PinePhone](https://www.pine64.org/pinephone/) was my first "real" Linux
phone: I had owned a Nexus 5 running [Ubuntu Touch](https://ubports.com/), but
it was basically an Android phone, running an Android kernel. While it was a
very fine device, definitely usable as a daily driver, I got quickly frustrated
by all the things I couldn't do on this system, mostly due to Canonical's design
choices: it was not a hacker's device, just a different, yet mainstream, mobile
phone.

Let's be honest, back when PINE64 announced the PinePhone, I had already
pre-ordered a [Librem 5](https://puri.sm/products/librem-5/), and I only
purchased this device so I could play with
[Phosh](https://puri.sm/posts/phosh-overview/) and the L5 software stack while
waiting for my "real" phone. But still, there was literally a world to build
with the PinePhone, so why not give it a try?

### Unboxing and early hacking

While waiting on the PinePhone, I gathered as much documentation as possible
about the device itself, but also the existing ecosystem: select developers had
access to an even earlier version of this phone, and there were already a few
systems available. Among those, [postmarketOS](https://postmarketos.org/) looked
the most promising, as it allowed the user to chose various environments,
[Plasma Mobile](https://www.plasma-mobile.org/) and Phosh being the ones I
absolutely wanted to try.

Test-driving the various OS's available at the time kept me busy for the major
part of the 2 evenings following the PinePhone delivery, and was enough to prove
the phone's potential. Still, the whole software stack was far less mature than
it is today, and a lot of features were missing (no suspend, no camera, to name
just a few), which was to be expected given the very small number of developers
having worked on it thus far. Nevertheless, the fact that it was actually usable
came in as a pleasant surprise.

But you can't get a tinkerer to simply use such a device. Therefore, armed with
a DIY serial adapter (probably the single most important tool of any embedded
software developer) made out of an old RCA-to-3.5mm-jack audio cable, it was
time to have a closer look at how this device worked, and what I could do with
it.

Being a strong [Debian](https://www.debian.org/) fan since the early 2000's, and
having some experience with creating custom Debian-based images for embedded
systems as part of $dayjob, I couldn't aim for anything other than running
Debian on my phone! Fortunately, the PINE64 and Debian communities had already
published everything I needed to succeed:

* the [PINE64 Gitlab](https://gitlab.com/pine64-org) contained the sources for
  the bootloader ([u-boot](https://gitlab.com/pine64-org/u-boot)) and
  [Linux kernel](https://gitlab.com/pine64-org/linux) used on their devices
* [debos](https://github.com/go-debos/debos), a simple yet powerful tool I was
  already familiar with, could be used to generate a base arm64 Debian system

As I was about to travel for the week-end, I simply packed the kernel and
bootloader sources on my laptop, along with as much documentation as I could
find, a postmarketOS image to serve as a reference, and a debos-generated
arm64 Debian rootfs.

Fun fact: the first step towards Mobian actually took place in the air,
as the first time I could get the PinePhone to boot a Debian-based image
happened during my flight to Brussels for last year's
[FOSDEM](https://fosdem.org/).

### Getting to a usable system

After the first successful boot and a bit of tinkering (running weston to make
sure the graphics driver was working, fiddling with `nmcli` to setup a wifi
connection -- which would prove essential as there was no other network
connectivity at that point), the next step was getting a usable graphical
environment working.

Back then, phosh was (and still is) being developed by [Purism](https://puri.sm)
for their Librem 5, and it was already running on the PinePhone with
postmarketOS. It already looked in good shape and was really appealing to me,
mostly as it's based on GTK and (as I would quickly discover) GNOME libraries.
Actually, my desktop of choice is XFCE, as I always found GNOME 3 to be a bit
laggy and unresponsive (it's been a while since I last tested it, though), but
we're still in GTK territory, which I, as both a user and a developer, have
always preferred over other toolkits.

As [PureOS](https://pureos.net/) (Purism's own distro) was based on Debian, I
first tried to simply add their repos to my base image and `apt install phosh`.
Unfortunately, I quickly found out it wouldn't be *that* easy, as PureOS was
based on an old(ish) version of Debian, and had many backported and/or modified
packages. In the end, using PureOS repos on a Debian system would effectively
have resulted in a [Frankendebian](https://wiki.debian.org/DontBreakDebian) of
the worst kind.

I could have used PureOS as my base system, and only add a small number of
packages on top of that, but my ultimate goal was running a "real" Debian on my
phone, including full access to the whole Debian archive. I then had to resort
to the option I tried to avoid: rebuilding the phosh ecosystem packages for
Debian.

Actually, I had hoped that *someone* would have done so, but it turned out there
was no such *someone* back then (remember it was still the *BraveHeart* days,
and PinePhones were only arriving into developers' hands). PureOS being based on
Debian, it only required minor adjustments to the existing packages to make them
build and run on a real Debian system. Purism really deserves credit here, for
developing Phosh and its ecosystem in the open, for using a Debian base for
their distro as it really lowered the amount of work needed to get everything
into Debian, and for doing their best at helping other distros.

Finally, after only a few days with the PinePhone, hacking mostly between FOSDEM
talks and during the Brussels round-trip, I was able to boot my PinePhone into
Phosh using a custom Debian-based image. A small cleanup and a few improvements
later, I published the initial
[git repo](https://gitlab.com/a-wai/debos-pinephone/-/tree/eb78b6bb3e937182c6335f1d0b3476f3472ffdc5)
of what would become Mobian on February 4th, 2020.

As you can see in the original
[README](https://gitlab.com/a-wai/debos-pinephone/-/blob/eb78b6bb3e937182c6335f1d0b3476f3472ffdc5/README.md)
file, there was much to do before reaching daily-driver status, but that's a
story for another day ;)

To be continued...
